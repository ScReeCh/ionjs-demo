var gulp = require('gulp'),
    cssnano = require('gulp-cssnano'),
    uglify = require('gulp-uglify'),
    concat = require('gulp-concat'),
    notify = require('gulp-notify'),
    cache = require('gulp-cache'),
    livereload = require('gulp-livereload'),
    del = require('del'),
    less = require('gulp-less'),
    path = require('path'),
    glob = require('glob');

// --------------------- Production  --------------------- //

/**
 * Compile theme LESS to CSS3
 */
gulp.task('less', function () {
    var stream = gulp.src("./src_assets/less/styles.less")
        .pipe(less({
            paths: [path.join(__dirname, 'less', 'includes')]
        }))
        .pipe(concat('styles.css'))
        // .pipe(cssnano())
        .pipe(gulp.dest('./assets/css'))
        .pipe(notify({
            message: '"LESS compile" task complete'
        }));
});


// configure which files to watch and what tasks to use on file changes
gulp.task('watch', function () {
    // Watch .js files
    // gulp.watch( 'node_modules/interact.js/**/*', [ 'development_js_modules' ] );
    gulp.watch('./src_assets/less/styles.less', ['less']);

    // Create LiveReload server
    // livereload.listen();

    // Watch any files in dist/, reload on change
    // gulp.watch(['public/**/*']).on('change', livereload.changed);

});
