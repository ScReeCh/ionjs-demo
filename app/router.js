const Router = require('ionjs/core/router');
const Authentication = require('./services/authentication/Authentication.js');

const SignIn = require('./components/signIn/SignIn.js');
const ExampleDashboard = require('./components/exampleDashboard/ExampleDashboard.js');

Router.setRoutes([{
    view: "SignIn",
    component: SignIn
}, {
    view: "ExampleDashboard",
    component: ExampleDashboard,
    authGuard: Authentication,
    default: true
}]);
