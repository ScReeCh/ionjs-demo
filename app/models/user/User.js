const RequestHttp = require('ionjs/core/http');
const Model = require('ionjs/core/model');

'use strict';

/**
 * DB Schema and DB file location
 * @type {Object}
 */
const DB = {
    'location': __dirname + '/../../../db/user.db',
    'schema': {
        'id': 'number',
        'first_name': 'string',
        'last_name': 'string',
        'email_address': 'string',
        'created_at': 'string',
        'created_at': 'string',
    }
};

module.exports = class User extends Model {

    constructor() {
        // Initiate DB shema for collection
        super(DB);
    }

    /**
     * Save user details to user model
     */
    save(data, callback) {
        console.log(data);
        this.insert(data, function (err, newData) {
            if (err) {
                callback(err, null);
                return false;
            }
            callback(null, newData);
        });
    }

    /**
     * Get all user is user model
     */
    getUser(callback) {
        this.find({}, function (err, docs) {
            if (err) {
                callback(err, null);
                return;
            }
            callback(null, docs);
        });
    }

    /**
     * Update single user
     */
    updateUser(id, payload, callback) {
        this.update({
            '_id': id
        }, payload, function (err, docs) {
            if (err) {
                callback(err, null);
                return;
            }
            callback(null, docs);
        });
    }
}
