const Router = require('ionjs/core/router');
const Highlight = require('highlight.js/lib/');

const User = require('../../models/user/User.js');

const SELECTOR = "content";
const TEMPLATE = "app/views/templates/saveDb/save-db";

'use strict';

var component = {
    "selector": SELECTOR,
    "template": TEMPLATE,
    "components": []
};

class SaveDb {
    constructor() {
        this.injectCode;
        this.currentUser;
        this.users = new User;
        this.getUserData();
    }

    getUserData() {
        this.users.getUser((err, response) => {
            if (err) {
                return false;
            }
            this.currentUser = response;
            this.injectCode = JSON.stringify(response);
            Highlight.initHighlighting();
        });
    }

    saveUserData() {
        console.log(this.parent.userDetails);
        this.users.updateUser(this.currentUser._id, {
            'first_name': this.parent.userDetails.first_name,
            'last_name': this.parent.userDetails.first_name
        }, (err, response) => {
            if (err) {
                console.log(err);
                return false;
            }
            this.getUserData();
        });
    }

    toCodeText(output) {

    }
}

component.view = SaveDb;

module.exports = component;
