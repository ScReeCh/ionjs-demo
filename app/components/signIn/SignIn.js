const Router = require('ionjs/core/router');

const User = require('../../models/user/User.js');

const SELECTOR = "main-window";
const TEMPLATE = "app/views/templates/signIn/sign-in";

'use strict';

var component = {
    "selector": SELECTOR,
    "template": TEMPLATE,
    "components": []
};

class SignIn {

    constructor() {
        this.users = new User;
        this.user = {
            first_name: '',
            last_name: ''
        }
    }

    signIn() {
        this.users.save(this.user, (err, response) => {
            if (err) {
                console.error(err);
                return;
            }
            console.warn(response);
            Router.navigate("ExampleDashboard");
        });
    }
}

component.view = SignIn;
module.exports = component;
