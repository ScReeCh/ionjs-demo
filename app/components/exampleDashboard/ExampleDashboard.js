'use strict';

const Router = require('ionjs/core/router');

const User = require('../../models/user/User.js');
const NavBar = require('../navBar/NavBar.js');
const FormsExample = require('../formsExample/FormsExample.js');
const Scrollbar = require('../scrollbar/Scrollbar.js');
const UsingThemes = require('../usingThemes/UsingThemes.js');
const Binding = require('../binding/Binding.js');
const SaveDb = require('../saveDb/SaveDb.js');

const SELECTOR = "main-window";
const TEMPLATE = "app/views/templates/exampleDashboard/example-dashboard";

var component = {
    "selector": SELECTOR,
    "template": TEMPLATE,
    "components": [
        NavBar,
        FormsExample,
        Scrollbar,
        UsingThemes,
        Binding,
        SaveDb
    ]
};

class ExampleDashboard {

    constructor() {
        this.userDetails;
        this.user = new User();
        this.user.getUser((err, response) => {
            if (err) {
                return;
            }
            this.userDetails = response[0];
        });
    }

    navigate(route) {
        Router.navigate(route, {
            param: "param_value"
        });
    }
}

component.view = ExampleDashboard;

module.exports = component;
