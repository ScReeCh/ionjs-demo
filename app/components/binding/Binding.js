const Router = require('ionjs/core/router');
const Assets = require('ionjs/core/assets');

const SELECTOR = "content";
const TEMPLATE = "app/views/templates/binding/binding";

'use strict';

var component = {
    "selector": SELECTOR,
    "template": TEMPLATE,
    "components": []
};

class Binding {
    constructor() {}

    onInit(node) {
        // this.parent.userDetails.first_name = 'Henco';
        // this.parent.userDetails.last_name = 'Burger';
    }
}

component.view = Binding;

module.exports = component;
