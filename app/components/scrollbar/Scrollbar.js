const Router = require('ionjs/core/router');

const SELECTOR = "content";
const TEMPLATE = "app/views/templates/scrollbar/scrollbar";

'use strict';

var component = {
    "selector": SELECTOR,
    "template": TEMPLATE,
    "components": []
};

class Scrollbar {
    constructor() {}

    onInit(node) {

        this.height;
        this.width;
        node.style.height = "200px";
        // this.perfectScrollbar.initialize(node);
        var scrollBar = Assets.scrollbar({
            element: node,
            settings: {
                wheelSpeed: 2,
                wheelPropagation: true,
                minScrollbarLength: 20
            },
            resize: (height, width) => {
                this.height = height;
                this.width = width;
            }
        });
        console.log([document.getElementsByTagName('content')]);
    }
}

component.view = Scrollbar;

module.exports = component;
