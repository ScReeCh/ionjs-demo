const Router = require('ionjs/core/router');

const SELECTOR = "content";
const TEMPLATE = "app/views/templates/usingThemes/using-themes";

'use strict';

var component = {
    "selector": SELECTOR,
    "template": TEMPLATE,
    "components": [],
    "active": true
};

class UsingThemes {
    constructor() {}
}

component.view = UsingThemes;

module.exports = component;
