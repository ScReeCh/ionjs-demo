const Router = require('ionjs/core/router');

const SELECTOR = "content";
const TEMPLATE = "app/views/templates/formsExample/forms-example";

'use strict';

var component = {
    "selector": SELECTOR,
    "template": TEMPLATE,
    "components": []
};

class FormsExample {
    constructor() {}

    onInit() {
        this.parent.userDetails.first_name = 'Henco';
        this.parent.userDetails.last_name = 'Burger';
    }
}

component.view = FormsExample;

module.exports = component;
