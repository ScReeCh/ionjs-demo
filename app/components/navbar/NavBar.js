const Router = require('ionjs/core/router');

const User = require('../../models/user/User.js');

const SELECTOR = "nav-bar";
const TEMPLATE = "app/views/templates/navBar/nav-bar";

'use strict';

var component = {
    "selector": SELECTOR,
    "template": TEMPLATE,
    "components": [],
    "active": true
};

class NavBar {
    constructor() {}
}

component.view = NavBar;

module.exports = component;
