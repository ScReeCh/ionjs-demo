'use strict';

const Auth = require('ionjs/core/authentication');
const Router = require('ionjs/core/router');

const User = require('../../models/user/User.js');

/**
 * Authentication serice.
 * Check to see if there is a logged in user saved.
 */
module.exports = class Authentication extends Auth {

    /**
     * Authentication placeholder
     */
    authenticate() {
        // User method
        this.user = new User;

        // Get saved user in user collection
        this.user.getUser((err, response) => {
            if (err) {
                // Navigate back to signin on error
                Router.navigate("SignIn");
                return;
            }
            if (response.length == 0) {
                // Navigate back to signin
                Router.navigate("SignIn");
                return;
            }
            // User logged in initiate authentication
            this.authenticated();
        });
    }
}
